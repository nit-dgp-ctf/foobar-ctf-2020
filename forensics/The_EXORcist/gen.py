def xor(a1, b1):
    encrpted = [ (a ^ b) for (a, b) in zip(a1, b1) ]
    return bytes(encrpted)

flag = b"GLUG{n0_fr33_r3d_bull}"

a = xor(flag, b'HellHellHellHellHellHellHell')
print(a.hex())