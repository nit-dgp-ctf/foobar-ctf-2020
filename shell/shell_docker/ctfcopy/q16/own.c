#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc){
    char buf[32] = {0x00};
    int key = 0x00;

    setbuf(stdout, NULL);

    printf("Key? ");
    scanf("%d", &key);

    int fd = key - 0x31337;
    int len = read(fd, buf, 32);

    if (!strcmp("GIMMEDAFLAG\n", buf)) {
        char * str = malloc(sizeof(char)*128);
        FILE *f = fopen("HIdDEn","r");
        fgets(str, 128, f);
        printf("%s", str);
        fclose(f);
        exit(0);
    }

    return 1;
}
