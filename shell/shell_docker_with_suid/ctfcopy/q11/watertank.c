#include <stdio.h>
#include <stdlib.h>

#define FLAG "HIDDEN"

int main(int argc, char **argv){
  
	gid_t gid = getegid();
	setresgid(gid, gid, gid);

	int numLitre = 0;

	char buffer[64];

	puts("Welcome to the Water Well problem!\n");
	puts("In order to get the flag, you will need to have 100L of water!\n");
	puts("So, how many litres of water are there in the well: ");
	fflush(stdout);
	gets(buffer);

	if (numLitre >= 100){
		printf("Congrats, you filled %d litres!\n", numLitre);
		printf("Here's your flag: %s\n", FLAG);
	} else {
		printf("Sorry, you only had filled %d litres, try again!\n",numLitre);
	}
		
	return 0;
}

